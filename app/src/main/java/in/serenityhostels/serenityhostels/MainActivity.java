package in.serenityhostels.serenityhostels;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.DownloadManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
TextView textView;
ImageButton imageButton;
EditText student_id,password;
ProgressBar progressBar;
String student_id_d,password_d;
CoordinatorLayout coordinatorLayout;
Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinator);
        student_id = (EditText)findViewById(R.id.s_id);
        password = (EditText)findViewById(R.id.password);
        textView = (TextView)findViewById(R.id.heading);
        button = (Button)findViewById(R.id.qr_login);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        imageButton = (ImageButton)findViewById(R.id.login_button);
        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/heading.ttf");
        textView.setTypeface(typeface);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            student_id_d = student_id.getText().toString();
            password_d = password.getText().toString();
            if((student_id_d.trim().isEmpty()) && ((password_d).trim()).isEmpty())
            {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Please Enter Your Login Credentials", Snackbar.LENGTH_LONG);
                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }else
            {
                if(student_id_d.equals("student") && password_d.equals("password"))
                {
                    Intent intent = new Intent(getApplicationContext(),Student_Main.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "INCORRECT EMAIL_ID PASSWORD", Snackbar.LENGTH_LONG);
                    // Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.RED);
                    snackbar.show();
                }
            }




            }
        });


    }
}
