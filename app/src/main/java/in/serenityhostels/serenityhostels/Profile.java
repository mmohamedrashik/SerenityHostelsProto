package in.serenityhostels.serenityhostels;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Mohammed Razik on 12/4/2015.
 */
public class Profile extends Fragment {
    View view;

    TextView student_id,dob,gender,mobile,email_id,college,course,fname,mname,p_mobile,pa_mobile,p_email,g_name,g_mobile,g_relation,address,g_address;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.profile,container,false);

        student_id = (TextView)view.findViewById(R.id.s_id);
        dob = (TextView)view.findViewById(R.id.dob);
        gender = (TextView)view.findViewById(R.id.gender);
        mobile = (TextView)view.findViewById(R.id.s_mobile);
        mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setMessage("CHOOSE YOUR OPTION TO CONTACT STUDENT");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "CALL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                String number = "tel:" + mobile.getText().toString();
                                try
                                {
                                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                    callIntent.setData(Uri.parse(number));
                                    startActivity(callIntent);
                                    dialog.cancel();
                                }catch (Exception e)
                                {
                                    Toast.makeText(getContext().getApplicationContext(),"CALL NOT SUPPORTED IN YOUR DEVICE",Toast.LENGTH_LONG).show();
                                }

                            }
                        });


                builder1.setNegativeButton(
                        "MESSAGE",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String number =  mobile.getText().toString();
                               try
                               {
                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                   smsIntent.setData(Uri.parse("smsto:"));
                                   smsIntent.setType("vnd.android-dir/mms-sms");
                                   smsIntent.putExtra("address", number);
                                   smsIntent.putExtra("sms_body"  , "");
                                   startActivity(smsIntent);
                               }catch (Exception e)
                               {
                                Toast.makeText(getContext().getApplicationContext(),"SMS SUPPORTED IN YOUR DEVICE",Toast.LENGTH_LONG).show();
                               }
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
        email_id = (TextView)view.findViewById(R.id.s_email);
        email_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = "mailto:"+email_id.getText().toString();
                try
                {
                    Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                    intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                    intent.setData(Uri.parse(data)); // or just "mailto:" for blank
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                    startActivity(intent);

                }catch (Exception e)
                {

                }

            }
        });
        college = (TextView)view.findViewById(R.id.college);
        course = (TextView)view.findViewById(R.id.course);
        fname = (TextView)view.findViewById(R.id.f_name);
        mname = (TextView)view.findViewById(R.id.m_name);
        p_mobile = (TextView)view.findViewById(R.id.p_mobile);
        p_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext().getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
        pa_mobile = (TextView)view.findViewById(R.id.pa_mobile);
        pa_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext().getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
        p_email =(TextView)view.findViewById(R.id.p_email);
        p_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext().getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
        g_name = (TextView)view.findViewById(R.id.g_name);
        g_mobile = (TextView)view.findViewById(R.id.g_mobile);
        g_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext().getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
        g_relation = (TextView)view.findViewById(R.id.g_relation);
        address = (TextView)view.findViewById(R.id.address);
        g_address = (TextView)view.findViewById(R.id.g_address);

        return view;
    }


}
